<?php

namespace DRK\DrkDonate\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Page\AssetCollector;

/**
 * MemberFormController
 */
class DonateController extends AbstractDrkController
{
    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        private readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * action send
     *
     * @return void
     */
    public function sendAction(): ResponseInterface
    {
        $this->view->assign('sending_ok', false);
        $this->view->assign('debug', $this->settings['debug']);
        $aArguments = $this->request->getArguments();

        $errors = false;

        if (empty($this->settings['business']) || !GeneralUtility::validEmail($this->settings['business'])) {
            $errors = true;
            $this->error = ['Error' => "Der PayPal-Accountname muss eine E-Mailadresse sein und darf nicht leer sein."];
        }

        $donateUrl = '';
        if (!empty($aArguments['amount'])) {
            $donateUrl = 'https://www.' . ($this->settings['sandbox'] ? 'sandbox.' : '') . 'paypal.com/cgi-bin/webscr';
            $donateUrl .= '?cmd=_donations';
            $donateUrl .= '&currency_code=EUR';

            $donateUrl .= '&business=' . urlencode($this->settings['business']);
            $donateUrl .= '&item_name=' . urlencode($this->settings['donateOrganisation']);
            $donateUrl .= '&item_number=' . urlencode($this->settings['donatePurpose']);
            $donateUrl .= '&amount=' . urlencode((stristr($aArguments['amount'], '.') && stristr(
                $aArguments['amount'],
                ','
            ) ? str_replace(',', '.', str_replace('.', '', $aArguments['amount'])) : str_replace(
                        ',',
                        '.',
                        $aArguments['amount']
                    )));

            if (!empty($this->settings['returnUrl'])) {
                if (is_numeric($this->settings['returnUrl'])) {
                    $this->uriBuilder->reset()
                        ->setTargetPageUid((int)$this->settings['returnUrl'])
                        ->setCreateAbsoluteUri(true);
                    if (GeneralUtility::getIndpEnv('TYPO3_SSL')) {
                        $this->uriBuilder->setAbsoluteUriScheme('https');
                    }
                    $returnUrl = $this->uriBuilder->build();
                } else {
                    $returnUrl = $this->settings['returnUrl'];
                }
                $donateUrl .= '&return=' . urlencode($returnUrl);
                unset($returnUrl);
            }

            if (!empty($this->settings['cancelUrl'])) {
                if (is_numeric($this->settings['cancelUrl'])) {
                    $this->uriBuilder->reset()
                        ->setTargetPageUid((int)$this->settings['cancelUrl'])
                        ->setCreateAbsoluteUri(true);
                    if (GeneralUtility::getIndpEnv('TYPO3_SSL')) {
                        $this->uriBuilder->setAbsoluteUriScheme('https');
                    }
                    $cancelUrl = $this->uriBuilder->build();
                } else {
                    $cancelUrl = $this->settings['cancelUrl'];
                }
                $donateUrl .= '&cancel_return=' . urlencode($cancelUrl);
                unset($cancelUrl);
            }
        } else {
            $errors = true;
            $this->error = ['Error' => "Der Betrag darf nicht leer sein."];
        }

        if (!$errors && $donateUrl != '') {
            $this->assetCollector->addInlineJavaScript('paypal-redirect', 'window.location.replace("'.$donateUrl.'");');
            $this->view->assign('donate_uri', $donateUrl);
            $this->view->assign('error', false);
        }
        // if Error, then show it now
        else {
            $this->view->assign('error', true);
            $this->error_reporting();
        }

        if ($this->settings['debug']) {
            $this->view->assign('debug', $this->request->getArguments());
        }

        return $this->htmlResponse();
    }

    /**
     * error_reporting
     */
    private function error_reporting(): void
    {
        $errors = $this->error;
        $this->view->assign('errorMessages', $errors);
    }

    /**
     * Init
     *
     * @return void
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        $this->assetCollector->addStyleSheet(
            'Donate',
            'EXT:drk_donate/Resources/Public/Css/styles.css'
        );

        $this->assetCollector->addJavaScript(
            'Donate',
            'EXT:drk_donate/Resources/Public/Scripts/tx_drkdonate.js',
            [],
            ['priority' => false] // lädt in footer

        );

    }

    /**
     * action show
     *
     * @return ResponseInterface
     */
    protected function showAction(): ResponseInterface
    {
        $errors = false;

        if (empty($this->settings['business']) || !GeneralUtility::validEmail($this->settings['business'])) {
            $errors = true;
            $this->error = ['Error' => "Der PayPal-Accountname muss eine E-Mailadresse sein und darf nicht leer sein."];
        }

        $amounts = explode(',', $this->settings['amounts']);
        foreach ($amounts as &$a) {
            trim($a);
        }

        if ($this->settings['returnUrl']) {
            $target = '_self';
        } else {
            $target = '_blank';
        }

        if (!$errors) {
            $this->view->assign('target', $target);
            $this->view->assign('amounts', $amounts);
            $this->view->assign('error', false);
        }
        // if Error, then show it now
        else {
            $this->view->assign('error', true);
            $this->error_reporting();
        }

        return $this->htmlResponse();
    }
}
