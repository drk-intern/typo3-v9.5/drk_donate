<?php

// Donate Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_donate',
    'Donate',
    'LLL:EXT:drk_donate/Resources/Private/Language/locallang_be.xlf:tt_content.donate_plugin.name',
    'EXT:drk_donate/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Spende (PayPal)'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);
