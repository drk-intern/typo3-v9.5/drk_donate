<?php

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_donate',
    'Donate',
    [
        \DRK\DrkDonate\Controller\DonateController::class => 'show, send',
    ],
    // non-cacheable actions
    [
        \DRK\DrkDonate\Controller\DonateController::class => 'send',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
